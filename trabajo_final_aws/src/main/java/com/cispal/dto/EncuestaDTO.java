package com.cispal.dto;

public class EncuestaDTO {
	private  int id;
	private String nombres;
	private String apellido;
	private String eleccion;
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEleccion() {
		return eleccion;
	}
	public void setEleccion(String eleccion) {
		this.eleccion = eleccion;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	

}
