package com.cispal.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cispal.model.Encuesta;

@Repository
public interface IEncuestaDao extends JpaRepository<Encuesta, Integer> {

}
