package com.cispal.model;

public enum Eleccion {
	JAVA(1,"JAVA"),C(1,"C#");
	
	private String nombre;
	private int id;
	
	private Eleccion(int id,String nombre) {
		this.id= id;
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public int getId() {
		return id;
	}
	

}
