package com.cispal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;

@SpringBootApplication
@EnableWebMvc
public class TrabajoFinalAwsApplication  {

	public static void main(String[] args) {
		SpringApplication.run(TrabajoFinalAwsApplication.class, args);
	}
	
	
	@Bean
	public AWSCognitoIdentityProviderClient cognitoClient() {
		AWSCognitoIdentityProviderClient cognitoClient = new AWSCognitoIdentityProviderClient( new DefaultAWSCredentialsProviderChain());
		cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_1));
		return cognitoClient;
	}
	
	

}
