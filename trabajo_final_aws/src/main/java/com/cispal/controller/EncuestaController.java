package com.cispal.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cispal.dto.EncuestaDTO;
import com.cispal.dto.RespuestaApi;
import com.cispal.model.Encuesta;
import com.cispal.service.IEncuestaService;



@RestController
@CrossOrigin
@RequestMapping(value = "api/encuesta")
public class EncuestaController {
	
	private static final Logger logger = LoggerFactory.getLogger(EncuestaController.class);
	
	@Autowired
	private IEncuestaService encuestaService;
	
	@PreAuthorize("hasRole('ADMINISTRADOR')")
	@GetMapping(value = "listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Encuesta>> listar(){
		try {
			return new ResponseEntity<List<Encuesta>>(encuestaService.listar(),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value="registrar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> registrar(@RequestBody EncuestaDTO encuesta){
		try {
			Encuesta en = new Encuesta();
			en.setNombres(encuesta.getNombres());
			en.setApellidos(encuesta.getApellido());
			en.setEleccion(encuesta.getEleccion());
			encuestaService.registrar(en);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@DeleteMapping(value="eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> eliminar(@PathVariable int id){
		try {
			encuestaService.eliminar(id);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
