package com.cispal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cispal.dao.IEncuestaDao;
import com.cispal.model.Encuesta;
import com.cispal.service.IEncuestaService;

@Service
public class EncuestaService implements IEncuestaService {
	
	@Autowired
	private IEncuestaDao encuestaDao;

	@Override
	public Encuesta registrar(Encuesta encuesta) {
		// TODO Auto-generated method stub
		return encuestaDao.save(encuesta);
	}

	@Override
	public Encuesta modificar(Encuesta t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(int id) {
		encuestaDao.deleteById(id);
		
	}

	@Override
	public Encuesta listarId(int id) {
		// TODO Auto-generated method stub
		return encuestaDao.findById(id).orElse(null);
	}

	@Override
	public List<Encuesta> listar() {
		// TODO Auto-generated method stub
		return encuestaDao.findAll();
	}

}
