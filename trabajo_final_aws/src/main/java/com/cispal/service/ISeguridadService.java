package com.cispal.service;

import com.cispal.dto.RenewPasswordFirstDTO;
import com.cispal.dto.RespuestaApi;
import com.cispal.dto.UpdatePasswordDTO;

public interface ISeguridadService {
	public RespuestaApi getToken(String username, String password);
	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword);
	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword);
	public RespuestaApi signOut(String token);
	public RespuestaApi refreshToken(String token);

}
