package com.cispal.service;

import java.util.List;

import com.cispal.model.Encuesta;

public interface IEncuestaService {
	
	public Encuesta registrar(Encuesta t);

	public Encuesta modificar(Encuesta t);

	public void eliminar(int id);

	public Encuesta listarId(int id);

	public List<Encuesta> listar();

}
